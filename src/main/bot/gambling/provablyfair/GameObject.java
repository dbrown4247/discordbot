package main.bot.gambling.provablyfair;

import main.util.GameController;
import main.util.Util;
import net.dv8tion.jda.core.entities.User;

import java.util.ArrayList;
import java.util.List;

public abstract class GameObject{
    private transient User user;
    protected transient List<RandomData<Integer>> rd;
    protected int gameObjectCount;

    public GameObject(User u, int gameObjectCount){
        this.user = u;
        this.gameObjectCount = gameObjectCount;
        rd = new ArrayList<>();
    }
    public abstract List<String> get();
    public abstract GameObject derive(User u);
    public User getUser(){
        return user;
    }

    public String getProofHash() {
        String proofHashes = "";
        for(RandomData i:rd){
            proofHashes += ":"+i.proofHash();
        }
        return Util.hash(proofHashes.substring(1));
    }

    public void logRandomData(){
        GameController.logGame(getProofHash().substring(0,32), rd);
    }
}