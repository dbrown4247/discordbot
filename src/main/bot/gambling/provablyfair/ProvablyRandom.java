package main.bot.gambling.provablyfair;

import main.util.GlobalSettings;
import main.util.Util;
import net.dv8tion.jda.core.entities.User;

import java.util.HashMap;
import java.util.Map;

public class ProvablyRandom  {
    private static java.security.SecureRandom sr = new java.security.SecureRandom();
    private static String serverSeed = Util.hash(sr.nextInt() + "");
    private static String serverSeedhash = Util.hash(serverSeed);
    private static Map<String, String> clientSeed = new HashMap<>();
    private static String defaultClientSeed = GlobalSettings.defaultSeed;
    private static int rollCount = 0;


    public static void setSeed(User user, String seed){
        clientSeed.put(user.getId(), seed);
    }

    public static String getSeed(User user){
        return (user != null && clientSeed.containsKey(user.getId())) ? clientSeed.get(user.getId()) : defaultClientSeed;
    }

    public static int getRollCount(){
        return rollCount;
    }

    public static final RandomData<Integer> nextInt(final int min, final int max, User user){
        final RandomData<Double> rd = nextDouble(user);
        final double mult = rd.get();

        return new RandomData<>(rd, (int)((double)(max-min)*mult)+min, new HashMap<String, Object>() {{
            put("mult",rd.get());
            put("min", min);
            put("max", max);
        }});
    }


    private static RandomData<Double> nextDouble(User user){
        if(rollCount >= 100){
            rollCount = 0;
            serverSeed = Util.hash(sr.nextInt() + "");
            serverSeedhash = Util.hash(serverSeed);
        }
        final String seed = getSeed(user);
        final int max = 0xFFFFF;
        String hash = Util.hash(serverSeed + seed + rollCount);
        int roll = Integer.parseInt(hash.substring(hash.length()-5), 16);
        rollCount++;
        return new RandomData<>(serverSeed, serverSeedhash, seed, rollCount,  (double)roll/max, null, user);
    }

    public static String getServerSeedHash(){
        return serverSeedhash;
    }

    public static boolean currentSeed(String tmp){
        return serverSeed.equals(tmp);
    }
}
