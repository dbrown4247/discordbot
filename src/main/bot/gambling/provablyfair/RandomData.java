package main.bot.gambling.provablyfair;

import main.util.Util;
import net.dv8tion.jda.core.entities.User;

import java.util.Map;

public class RandomData <T>{
    private String serverSeed;
    private String serverSeedHash;
    private String clientSeed;
    private transient User user;
    private Map<String, Object> params;
    private transient T next;
    private int rollCount;

    public RandomData(RandomData rd, T next, Map<String, Object> params){
        this.serverSeed = rd.getServerSeed();
        this.serverSeedHash = rd.getServerSeedHash();
        this.clientSeed = rd.getClientSeed();
        this.user = rd.getUser();
        this.rollCount = rd.rollCount;
        this.next = next;
        this.params = params;
    }

    public RandomData(String serverSeed, String serverSeedHash, String clientSeed, int rollCount, T next, Map<String, Object> params, User user){
        this.serverSeed = serverSeed;
        this.serverSeedHash = serverSeedHash;
        this.clientSeed = clientSeed;
        this.next = next;
        this.params = params;
        this.user = user;
        this.rollCount = rollCount;
    }

    public String proofHash(){
        return Util.hash(serverSeedHash + ":" + clientSeed + ":" + rollCount);
    }

    public User getUser(){
        return this.user;
    }

    public String getServerSeed(){
        return serverSeed;
    }

    public boolean currentSeed(){
        return ProvablyRandom.currentSeed(serverSeed);
    }

    public int getRollCount(){
        return this.rollCount;
    }

    public String getServerSeedHash(){
        return serverSeedHash;
    }

    public Map<String, Object> getParams(){
        return params;
    }

    public String getClientSeed(){
        return clientSeed;
    }

    public T get(){
        return next;
    }
}