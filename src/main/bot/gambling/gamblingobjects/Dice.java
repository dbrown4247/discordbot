package main.bot.gambling.gamblingobjects;

import main.bot.gambling.provablyfair.GameObject;
import main.bot.gambling.provablyfair.ProvablyRandom;
import main.bot.gambling.provablyfair.RandomData;
import net.dv8tion.jda.core.entities.User;

import java.util.ArrayList;
import java.util.List;

public class Dice extends GameObject {

    private int min;
    private int max;
    private transient int roll = 0;

    public Dice(User u, int min, int max, int gameObjectCount) {
        super(u, gameObjectCount);
        this.min = min;
        this.max = max;
        for (int i = 0; i < gameObjectCount; i++){
            RandomData<Integer> tmp = ProvablyRandom.nextInt(min, max, u);
            rd.add(tmp);
            roll += tmp.get();
        }
    }

    @Override
    public List<String> get() {
        return new ArrayList<String>(){{
            add(roll+"");
        }};
    }

    @Override
    public GameObject derive(User u) {
        return new Dice(u, min, max, gameObjectCount);
    }
}
