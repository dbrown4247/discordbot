package main.bot.gambling.gamblingobjects;

import main.bot.gambling.provablyfair.GameObject;
import main.bot.gambling.provablyfair.ProvablyRandom;
import main.bot.gambling.provablyfair.RandomData;
import net.dv8tion.jda.core.entities.User;

import java.util.ArrayList;
import java.util.List;


public class Box extends GameObject{
    private BoxItem[] contents;
    private boolean removable;
    private transient boolean[] removed;
    public Box(User u, BoxItem[] contents, boolean removable, int gameObjectCount){
        super(u, gameObjectCount);
        this.contents = contents;
        this.removed = new boolean[contents.length];
        this.removable = removable;
        int count = 0;
        for(BoxItem i:contents){
            count += i.getWeight();
        }
        for (int i = 0; i < gameObjectCount; i++){
            rd.add(ProvablyRandom.nextInt(0, count, u));
        }
    }

    @Override
    public List<String> get() {
        List<String> output = new ArrayList<>();

        for(RandomData<Integer> roll: rd) {
            int count = 0;
            int index = 0;
            for (BoxItem i : contents) {
                if (roll.get() <= count + i.getWeight() && (!removable || !removed[index])) {
                    removed[index] = true;
                    output.add(i.getLabel());
                    break;
                }
                count += i.getWeight();
                index++;
            }
        }
        return output;
    }

    @Override
    public Box derive(User u){
        return new Box(u, this.contents, this.removable, gameObjectCount);
    }

}
