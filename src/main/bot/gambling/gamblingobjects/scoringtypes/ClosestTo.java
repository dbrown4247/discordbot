package main.bot.gambling.gamblingobjects.scoringtypes;

import main.bot.gambling.gamblingobjects.ScoringInterface;

import java.util.List;

public class ClosestTo extends ScoringInterface {
    private int target;
    public ClosestTo(int target){
        this.target = target;
    }

    @Override
    public boolean relativeScorer() {
        return true;
    }

    @Override
    public int getScore(List<String> resultSet) {
        return Math.abs(Integer.MAX_VALUE - target - Integer.parseInt(resultSet.get(0)));
    }
}
