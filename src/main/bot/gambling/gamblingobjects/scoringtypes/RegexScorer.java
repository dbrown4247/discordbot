package main.bot.gambling.gamblingobjects.scoringtypes;

import main.bot.gambling.gamblingobjects.ScoringInterface;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexScorer extends ScoringInterface {

    private List<String> regexPatterns;

    public RegexScorer(List<String> regexPatterns){
        this.regexPatterns = regexPatterns;
    }

    @Override
    public boolean relativeScorer() {
        return true;
    }

    @Override
    public int getScore(List<String> resultSet) {
        resultSet.sort((o1, o2) -> {
            char[] c1 = o1.toCharArray();
            char[] c2 = o2.toCharArray();
            for(int i = 0; i < c1.length; i++){
                if(c1[i] == c2[i]){
                    continue;
                }else if(c1[i] > c2[i]){
                    return 1;
                }else{
                    return -1;
                }
            }
            return 0;
        });
        String compString = String.join(":", resultSet).toLowerCase();
        int failedPatterns = 0;
        for(String i:regexPatterns){
            Pattern p = Pattern.compile(i);
            Matcher m = p.matcher(compString);
            if(m.find()){
                break;
            }else{
                failedPatterns++;
            }
        }
        return Integer.MAX_VALUE - failedPatterns;
    }
}
