package main.bot.gambling.gamblingobjects.scoringtypes;

import main.bot.gambling.gamblingobjects.ScoringInterface;

import java.util.List;

public class AtLeast extends ScoringInterface {

    public int minimum;

    public AtLeast(int target){
        this.minimum = target;
    }

    @Override
    public boolean relativeScorer() {
        return false;
    }

    @Override
    public int getScore(List<String> resultSet) {
        return Integer.parseInt(resultSet.get(0)) >= minimum ? Integer.MAX_VALUE : 0;
    }
}
