package main.bot.gambling.gamblingobjects;

import java.util.List;

public abstract class ScoringInterface {

    public ScoringInterface(){

    }

    public abstract boolean relativeScorer();

    public abstract int getScore(List<String> resultSet);
}
