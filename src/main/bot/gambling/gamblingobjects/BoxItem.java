package main.bot.gambling.gamblingobjects;

public class BoxItem {
    private int weight;
    private String label;
    private int value;
    public BoxItem(int weight, String label, int value){
        this.weight = weight;
        this.label = label;
        this.value = value;
    }

    public String getLabel(){
        return this.label;
    }

    public int getWeight(){
        return this.weight;
    }

    @Override
    public String toString() {
        return label;
    }
}
