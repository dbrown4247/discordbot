package main.bot.command;

import com.sun.deploy.util.StringUtils;
import main.bot.gambling.gamblingobjects.ScoringInterface;
import main.bot.gambling.provablyfair.GameObject;
import main.util.GameController;
import main.util.Util;
import main.util.exceptions.GameObjectNotFoundException;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class AbstractGame extends Command {

    String gameName;
    String rollPrefix;
    String rollSuffix;
    String gameImage;
    String gameObjectName;
    String repeatObjectDelimiter;
    GameObject gameObject;
    ScoringInterface scorer;


    public AbstractGame(String syntax, String format, String gameName, String rollPrefix, String rollSuffix, String gameImage, String gameObjectName, String repeatObjectDelimiter, ScoringInterface scorer, String... prefix) throws GameObjectNotFoundException {
        super(syntax, format, prefix);
        this.gameName = gameName;
        this.rollPrefix = rollPrefix;
        this.rollSuffix = rollSuffix;
        this.gameImage = gameImage;
        this.gameObjectName = gameObjectName;
        this.scorer = scorer;
        this.repeatObjectDelimiter = repeatObjectDelimiter;
        this.gameObject = GameController.getGameObject(gameObjectName);
    }

    @Override
    public boolean canUse(User a) {
        return true;
    }

    @Override
    public void execute(MessageReceivedEvent cmd) {
        final EmbedBuilder builder = Util.getGameEmbedBuilder(gameName);
        final List<GameObject> randomObject = new ArrayList<>();
        if(gameObject == null){
            gameObject = GameController.getGameObject(gameObjectName);
            if(gameObject == null) {
                System.out.println("GameObject not found: "+gameObjectName);
                return;
            }
        }
        if (cmd.getMessage().getMentionedUsers().size() > 0) {
            cmd.getMessage().getMentionedUsers().forEach(u -> {
                GameObject tmp = gameObject.derive(u);
                builder.addField(u.getName(), rollPrefix + StringUtils.join(tmp.get(), repeatObjectDelimiter) + rollSuffix, true);
                randomObject.add(tmp);
            });
        }else {
            GameObject tmp = gameObject.derive(cmd.getAuthor());
            builder.addField(cmd.getAuthor().getName(), rollPrefix + StringUtils.join(tmp.get(), repeatObjectDelimiter) + rollSuffix, true);
            randomObject.add(tmp);
        }
        List<GameObject> winners = new ArrayList<>();
        if(randomObject.size() > 1) {
            for (GameObject i : randomObject) {
                if (scorer != null) {
                    if (scorer.relativeScorer()) {
                        if (winners.size() == 0) {
                            winners.add(i);
                        } else {
                            if (scorer.getScore(i.get()) > scorer.getScore(winners.get(0).get())) {
                                winners.clear();
                                winners.add(i);
                            } else if (scorer.getScore(i.get()) == scorer.getScore(winners.get(0).get())) {
                                winners.add(i);
                            }
                        }
                    } else {
                        if (scorer.getScore(i.get()) == Integer.MAX_VALUE) {
                            winners.add(i);
                        }
                    }
                }
            }
        }else{
            if(!scorer.relativeScorer() && randomObject.size() == 1){
                GameObject gameobject = randomObject.get(0);
                if (scorer.getScore(gameobject.get()) == Integer.MAX_VALUE) {
                    winners.add(gameobject);
                }
            }
        }
        if (winners.size() == 1) {
            builder.addField("", "Congratulations " + winners.get(0).getUser().getAsMention(), false);
        } else if (winners.size() > 1) {
            String content = winners.size() + " Way tie!";
            for (GameObject i : winners) {
                content += " " + i.getUser().getAsMention();
            }
            builder.addField("", content, false);
        }
        String proofs = "";
        for (GameObject i : randomObject) {
            proofs += i.getProofHash().substring(0, 32) + " | ";
            i.logRandomData();
        }
        proofs = proofs.substring(0, proofs.length() - 3);
        builder.setFooter("#" + proofs, null);
        builder.setThumbnail(gameImage);
        cmd.getChannel().sendMessage(builder.build()).queue();
    }

}
