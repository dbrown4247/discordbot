package main.bot.command.commands;

import main.bot.command.Command;
import main.bot.gambling.provablyfair.ProvablyRandom;
import main.bot.gambling.provablyfair.RandomData;
import main.util.GameController;
import main.util.GlobalSettings;
import main.util.Util;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.List;

public class Proof extends Command {
    public Proof() {
        super("!proof", "!proof [#hash]", "!proof");
    }

    @Override
    public boolean canUse(User a) {
        return true;
    }

    @Override
    public void execute(MessageReceivedEvent cmd) {
        final EmbedBuilder builder = Util.getGameEmbedBuilder("Game Info");
        if(cmd.getMessage().getContent().indexOf(" ") > 0) {
            String partialHash = cmd.getMessage().getContent().substring(cmd.getMessage().getContent().indexOf(" ")+1);
            List<RandomData<Integer>> gameData = GameController.getGameData(partialHash);
            if(gameData != null) {
                String lastServerSeedHash = "";
                String lastClientSeed = "";
                String range = "";
                for(RandomData i:gameData) {
                    if(!lastServerSeedHash.equals(i.getServerSeedHash())) {
                        builder.addField("Server Seed Hash", (lastServerSeedHash = i.getServerSeedHash()), false);
                        if(!ProvablyRandom.currentSeed(i.getServerSeed())) {
                            builder.addField("Server Seed", i.getServerSeed(), false);
                        }else{
                            builder.addField("Server seed", "Server hidden", false);
                        }
                    }
                    if(!lastClientSeed.equals(i.getClientSeed())) {
                        builder.addField("Client Seed", (lastClientSeed = i.getClientSeed()), false);
                    }
                    if(!range.equals(i.getParams().get("min") + "-" + i.getParams().get("max"))) {
                        builder.addField("Range", (range = (i.getParams().get("min") + "-" + i.getParams().get("max"))), false);
                    }
                    builder.addField("Roll count", i.getRollCount() + "", true);
                    builder.addField("Roll", (int)(
                            (double)i.getParams().get("min") + ((double)i.getParams().get("mult") * ((double)i.getParams().get("max") - (double)i.getParams().get("min")))
                    )+" ("+i.getParams().get("mult").toString().substring(0, 5)+")", true);
                }
            }else{
                builder.setDescription("No game found with partial hash: "+partialHash);
            }
        }else{
            builder.setTitle("Verification");
            builder.addField("Step 1:","SHA-512(serverSeed clientSeed rollcount)", false);
            builder.addField("Step 2:", "Treat the last 3 characters as a hexadecimal number", false);
            builder.addField("Step 3:", "Divide the number in step 2 by 1048575 (0xFFFFF)", false);
            builder.addField("Step 4:", "Multiply this number by the number range of the game and add the lower bound to get your roll", false);
            builder.addField("Pseudo code:", "((sha512(serverSeed + clientSeed + nonce).substring(hash.length()-5)/0xFFFFF) * (upperBound - lowerBound)) + lowerBound", false);

            builder.setDescription("The server seed is revealed every 100 rolls.");
        }
        builder.setFooter("#Provably Fair", null);
        builder.setThumbnail(GlobalSettings.brandIcon);

        cmd.getChannel().sendMessage(builder.build()).queue();
    }
}
