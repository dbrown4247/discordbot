package main.bot.command.commands;

import main.bot.command.Command;
import main.bot.gambling.provablyfair.ProvablyRandom;
import main.util.GlobalSettings;
import main.util.Util;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class Seed extends Command {
    public Seed() {
        super("!seed", "!seed [clientseed]", "!seed");
    }

    @Override
    public boolean canUse(User a) {
        return true;
    }

    @Override
    public void execute(MessageReceivedEvent cmd) {
        final EmbedBuilder builder = Util.getGameEmbedBuilder("Game Info");
        if(cmd.getMessage().getContent().indexOf(" ") > 0){
            String newSeed = cmd.getMessage().getContent().substring(cmd.getMessage().getContent().indexOf(" ")+1);
            ProvablyRandom.setSeed(cmd.getAuthor(), newSeed);
            builder.setDescription("Your client seed has been set to: "+newSeed);
        }else{
            builder.addField("Server Seed Hash", ProvablyRandom.getServerSeedHash(), false);
            builder.addField("Client Seed", ProvablyRandom.getSeed(cmd.getAuthor()), false);
            builder.addField("Roll Count", ProvablyRandom.getRollCount()+"", false);
        }

        builder.setFooter("#Provably Fair", null);
        builder.setThumbnail(GlobalSettings.brandIcon);

        cmd.getChannel().sendMessage(builder.build()).queue();
    }
}