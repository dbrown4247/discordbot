package main.bot.command;

import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Command {
    private String[] prefix;
    private String syntax;
    private String format;

    public abstract boolean canUse(User a);
    public abstract void execute(MessageReceivedEvent cmd);

    public Command(String syntax, String format, String... prefix){
        this.prefix = prefix;
        this.syntax = syntax;
        this.format = format;
    }

    public boolean validate(MessageReceivedEvent cmd){
        if(matchPrefix(cmd.getMessage().getContent())){
            if(canUse(cmd.getAuthor())){
                if(validSyntax(cmd.getMessage().getContent())){
                    execute(cmd);
                }else{
                    badSyntaxError();
                }
            }else{
                String tmp = canUseFailMessage();
                if(tmp != null) {
                    cmd.getChannel().sendMessage(tmp).complete();
                }
            }
        }
        return false;
    }

    public String badSyntaxError(){
        return "Bad syntax please use: "+syntax;
    }
    public String canUseFailMessage(){
        return null;
    }
    public boolean matchPrefix(String cmd){
        String tmp = getCmdPrefix(cmd);
        for(String i:prefix){
            if(i.equals(tmp)){
                System.out.println(i+":"+tmp);
                return true;
            }
        }
        return false;
    }
    public static String getCmdPrefix(String cmd){
        return cmd.indexOf(" ") > 0 ? cmd.substring(0,cmd.indexOf(" ")) : cmd;
    }
    public String getSyntax(){
        return this.syntax;
    }
    public String[] getPrefix(){
        return this.prefix;
    }
    public boolean validSyntax(String cmd){
        for(String i:prefix) {
            String tmp = replaceFirst1(syntax,prefix[0], i);
            Pattern p = Pattern.compile(tmp, Pattern.LITERAL);
            Matcher m = p.matcher(cmd);
            if(m.find()){
                return true;
            }
        }
        return false;
    }

    private static String replaceFirst1(CharSequence source, CharSequence target, CharSequence replacement) {
        return Pattern.compile(target.toString(), Pattern.LITERAL).matcher(
                source).replaceFirst(Matcher.quoteReplacement(replacement.toString()));
    }

    public String getFormat() {
        return format;
    }
}
