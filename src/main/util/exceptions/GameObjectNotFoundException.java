package main.util.exceptions;

public class GameObjectNotFoundException extends Throwable{
    public GameObjectNotFoundException(String name){
        System.out.println("No game with name found: "+name);
    }
}
