package main.util;

import net.dv8tion.jda.core.EmbedBuilder;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.OffsetDateTime;

public class Util {
    public static String hash(String input){
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-512");
            md.update((input).toString().getBytes("UTF-8")); // Change this to "UTF-16" if needed
            byte[] digest = md.digest();
            return String.format("%064x", new java.math.BigInteger(1, digest));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static EmbedBuilder getGameEmbedBuilder(String name) {
        EmbedBuilder builder = new EmbedBuilder();

        builder.setAuthor(name, GlobalSettings.brandIcon, GlobalSettings.brandIcon);

        builder.setThumbnail(GlobalSettings.brandIcon);
        builder.setColor(GlobalSettings.accentColor);
        builder.setTimestamp(OffsetDateTime.now());

        return builder;
    }

    public static String getDataUri(String suffix){
        return System.getProperty("user.home")+"\\GamblePro\\"+suffix;
    }


}
