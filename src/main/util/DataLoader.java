package main.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.typeadapters.RuntimeTypeAdapterFactory;
import main.bot.command.AbstractGame;
import main.bot.command.Command;
import main.bot.gambling.gamblingobjects.Box;
import main.bot.gambling.gamblingobjects.BoxItem;
import main.bot.gambling.gamblingobjects.Dice;
import main.bot.gambling.gamblingobjects.ScoringInterface;
import main.bot.gambling.gamblingobjects.scoringtypes.AtLeast;
import main.bot.gambling.gamblingobjects.scoringtypes.ClosestTo;
import main.bot.gambling.gamblingobjects.scoringtypes.RegexScorer;
import main.bot.gambling.provablyfair.GameObject;
import main.util.data.GameLog;
import main.util.exceptions.GameObjectNotFoundException;
import main.util.data.Player;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DataLoader {

    private static final Map<String, GameObject> defaultGameObjects = new HashMap<String, GameObject>(){{
        put("dragoncrate", new Box(null, new BoxItem[]{
                new BoxItem(1, "Dragon med helm", 59000),
                new BoxItem(1, "Dragon sq shield", 300000),
                new BoxItem(1, "Dragon dagger", 17000),
                new BoxItem(1, "Dragon spear", 37000),
                new BoxItem(1, "Dragon longsword", 59000),
                new BoxItem(1, "Dragon battleaxe", 119000),
                new BoxItem(1, "Dragon mace", 29000),
                new BoxItem(1, "Dragon chainbody", 675000),
                new BoxItem(1, "Dragon halberd", 149000),
                new BoxItem(1, "Dragon platelegs", 161000),
                new BoxItem(1, "Dragon plateskirt", 161000),
                new BoxItem(1, "Dragon scimitar", 59000)
        }, false, 1));
        put("testbox", new Box(null, new BoxItem[]{
                new BoxItem(5, "5", 0),
                new BoxItem(4, "4", 0),
                new BoxItem(1, "1", 0),
        }, false, 1));
        put("d6", new Dice(null, 1, 6, 1));
        put("d100", new Dice(null, 1, 100, 1));
        put("2d6", new Dice(null, 1, 6, 2));
        put("d12", new Dice(null, 1, 6, 1));
        BoxItem[] tmp;
        put("flower", new Box(null, (tmp = new BoxItem[]{
                new BoxItem(3491, "Red", 0),
                new BoxItem(3487, "Orange", 0),
                new BoxItem(3046, "Yellow", 0),
                new BoxItem(3898, "Blue", 0),
                new BoxItem(3830, "Purple", 0),
                new BoxItem(2565, "Pastel", 0),
                new BoxItem(3871, "Rainbow", 0),
                new BoxItem(29,"White", 0),
                new BoxItem(50,"Black", 0),
        }), false, 1));
        put("flowerpoker", new Box(null, tmp, false, 5));
    }};

    private static final Map<String, AbstractGame> defaultGames = new HashMap<String, AbstractGame>(){{
        try {
            put("!flowerpoker", new AbstractGame("!fp", "!fp [@data]...", "Flower Poker", "", " ","http://i.imgur.com/7p428al.png", "flowerpoker", " ", new RegexScorer(new ArrayList<String>(){{
                add("([^:]+)(:\\1){4}");//5OAK
                add("([^:]+)(:\\1){3}");//4OAK
                add("(?:([^:]+)(?::\\1){1}:([^:]+)(?::\\2){2})|(?:([^:]+)(?::\\3){2}:([^:]+)(?::\\4){1})");//FH
                add("([^:]+)(:\\1){2}");//3OAK
                add("(?:([^:]+)(?::\\1){1}:(?:[^:]+:)*?([^:]+)(?::\\2){1})");//2P
                add("([^:]+)(:\\1){1}");//1P
            }}),"!flowerpoker", "!fp"));
            put("!plant", new AbstractGame("!plant", "!plant [@players]...", "Mithril Seeds", "plants a ", " flower", "http://i.imgur.com/7p428al.png", "flower", " ", null, "!plant"));
            put("!testbox", new AbstractGame("!testbox", "!testbox [@data]...", "test", "prefix test", " suffix test",  "http://i.imgur.com/7p428al.png", "testbox", " ", null,"!testbox"));
            put("!dicetest", new AbstractGame("!dicetest", "!dicetest [@data]...", "test", "prefix test", " suffix test",  "http://i.imgur.com/7p428al.png", "d6", " ", null, "!dicetest"));
            put("!55x2", new AbstractGame("!55x2", "!55x2 [@players]...", "55x2", "rolls a ", " on the 100 sided die",  "http://i.imgur.com/7p428al.png", "d100", " ", new AtLeast(55), "!55x2"));
        } catch (GameObjectNotFoundException e) {
            e.printStackTrace();
        }
    }};

    public static Map<String, GameObject> loadGameObjects(){
        Map<String, GameObject> gameObjects = new HashMap<>();
        if(GlobalSettings.generateDefaultGameObjects){
            writeDefaultGameObjects();
        }
        File file = new File(Util.getDataUri("gameobjects"));
        for (File i : file.listFiles()) {
            try (InputStream reader = new FileInputStream(i)) {
                RuntimeTypeAdapterFactory<ScoringInterface> scorerAdapter = RuntimeTypeAdapterFactory.of(ScoringInterface.class).registerSubtype(RegexScorer.class).registerSubtype(AtLeast.class).registerSubtype(ClosestTo.class);
                RuntimeTypeAdapterFactory<GameObject> gameObjectAdapter = RuntimeTypeAdapterFactory.of(GameObject.class).registerSubtype(Dice.class).registerSubtype(Box.class);

                Gson gson = (new GsonBuilder().setPrettyPrinting().registerTypeAdapterFactory(gameObjectAdapter).registerTypeAdapterFactory(scorerAdapter)).create();
                String input = inputStreamToString(reader);
                gameObjects.put(i.getName().substring(0, i.getName().length()-5), gson.fromJson(input, GameObject.class));
                System.out.println("GameObject loaded: " + i.getName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return gameObjects;
    }

    public static Map<String, Command> loadGames(){
        Map<String, Command> games = new HashMap<>();
        if(GlobalSettings.generateDefaultGames){
            writeDefaultGames();
        }
        File file = new File(Util.getDataUri("games"));
        for (File i : file.listFiles()) {
            try (InputStream reader = new FileInputStream(i)) {
                RuntimeTypeAdapterFactory<ScoringInterface> scorerAdapter = RuntimeTypeAdapterFactory.of(ScoringInterface.class).registerSubtype(RegexScorer.class).registerSubtype(AtLeast.class).registerSubtype(ClosestTo.class);
                RuntimeTypeAdapterFactory<GameObject> gameObjectAdapter = RuntimeTypeAdapterFactory.of(GameObject.class).registerSubtype(Dice.class).registerSubtype(Box.class);

                Gson gson = (new GsonBuilder().setPrettyPrinting().registerTypeAdapterFactory(gameObjectAdapter).registerTypeAdapterFactory(scorerAdapter)).create();
                String input = inputStreamToString(reader);
                AbstractGame game = gson.fromJson(input, AbstractGame.class);
                for(String prefix:game.getPrefix()) {
                    games.put(prefix, game);
                }
                System.out.println("Games loaded: " + i.getName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return games;
    }

    public static String inputStreamToString(InputStream inputStream) throws IOException {
        try(ByteArrayOutputStream result = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) != -1) {
                result.write(buffer, 0, length);
            }

            return result.toString("UTF-8");
        }
    }

    private static Map<String,AbstractGame>  writeDefaultGames() {
        Map<String,AbstractGame> games = new HashMap<>();
        defaultGames.forEach((s, game) -> {
            File file = new File(Util.getDataUri("games\\"+s+".json"));
            if(!file.exists()){
                file.getParentFile().mkdirs();
            }
            try (Writer writer = new OutputStreamWriter(new FileOutputStream(file))) {
                RuntimeTypeAdapterFactory<ScoringInterface> scorerAdapter = RuntimeTypeAdapterFactory.of(ScoringInterface.class).registerSubtype(RegexScorer.class).registerSubtype(AtLeast.class).registerSubtype(ClosestTo.class);
                RuntimeTypeAdapterFactory<GameObject> gameObjectAdapter = RuntimeTypeAdapterFactory.of(GameObject.class).registerSubtype(Dice.class).registerSubtype(Box.class);

                Gson gson = (new GsonBuilder().setPrettyPrinting().registerTypeAdapterFactory(gameObjectAdapter).registerTypeAdapterFactory(scorerAdapter)).create();
                gson.toJson(game, AbstractGame.class, writer);
                System.out.println("Game written: "+s);
                games.put(s, game);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return games;
    }

    private static Map<String,GameObject> writeDefaultGameObjects(){
        Map<String,GameObject> gameObjects = new HashMap<>();
        defaultGameObjects.forEach((s, gameObject) -> {
            File file = new File(Util.getDataUri("gameobjects\\"+s+".json"));
            if(!file.exists()){
                file.getParentFile().mkdirs();
            }
            try (Writer writer = new OutputStreamWriter(new FileOutputStream(file))) {
                RuntimeTypeAdapterFactory<ScoringInterface> scorerAdapter = RuntimeTypeAdapterFactory.of(ScoringInterface.class).registerSubtype(RegexScorer.class).registerSubtype(AtLeast.class).registerSubtype(ClosestTo.class);
                RuntimeTypeAdapterFactory<GameObject> gameObjectAdapter = RuntimeTypeAdapterFactory.of(GameObject.class).registerSubtype(Dice.class).registerSubtype(Box.class);

                Gson gson = (new GsonBuilder().setPrettyPrinting().registerTypeAdapterFactory(gameObjectAdapter).registerTypeAdapterFactory(scorerAdapter)).create();
                gson.toJson(gameObject, GameObject.class, writer);
                System.out.println("GameObject written: "+s);
                gameObjects.put(s, gameObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return gameObjects;
    }

    public static Player loadPlayer(String id){
        File file = new File(Util.getDataUri("players\\"+id+".json"));
        try (InputStream reader = new FileInputStream(file)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String input = inputStreamToString(reader);
            Player player = gson.fromJson(input, Player.class);
            System.out.println(player.getName()+" loaded");
            return player;
        } catch (Exception e) {
            return null;
        }
    }

    public static GameLog loadGame(String partialHash){
        File file = new File(Util.getDataUri("gameLog\\"+partialHash+".json"));
        try (InputStream reader = new FileInputStream(file)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String input = inputStreamToString(reader);
            GameLog gameLog = gson.fromJson(input, GameLog.class);
            System.out.println(partialHash+": game loaded");
            return gameLog;
        } catch (Exception e) {
            return null;
        }
    }

    public static void writeGame(GameLog gameLog){
        File file = new File(Util.getDataUri("gameLog\\"+gameLog.getPartialHash()+".json"));
        if(!file.exists()){
            file.getParentFile().mkdirs();
        }
        try (Writer writer = new OutputStreamWriter(new FileOutputStream(file))) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            gson.toJson(gameLog, GameLog.class, writer);
            System.out.println("GameObject written: "+gameLog.getPartialHash());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
