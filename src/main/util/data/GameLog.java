package main.util.data;

import main.bot.gambling.provablyfair.RandomData;

import java.util.List;

public class GameLog {
    private long endTime;
    private String partialHash;
    private List<RandomData<Integer>> rd;

    public GameLog(String partialHash, List<RandomData<Integer>> rd){
        this.partialHash = partialHash;
        this.rd = rd;
        this.endTime = System.currentTimeMillis();
    }

    public String getPartialHash() {
        return this.partialHash;
    }

    public List<RandomData<Integer>> getRd() {
        return rd;
    }
}
