package main.util.data;

import main.util.DataLoader;
import main.util.GlobalSettings;
import net.dv8tion.jda.core.entities.User;

public class Player {
    private String userID;
    private String name;
    private String discriminator;

    private long tokenCount;
    private long freeTokenCount;

    private long lifeTimeWon;
    private long listTimeLost;
    private long totalBet;
    private long firstSeen;

    private long lastSeen;

    public static Player getPlayer(User u){
        Player tmp = DataLoader.loadPlayer(u.getId());
        if(tmp == null){
            return new Player(u.getId(), u.getName(), u.getDiscriminator(), GlobalSettings.initialTokenCount, GlobalSettings.initialFreeTokenCount, 0, 0, 0, System.currentTimeMillis(), System.currentTimeMillis());
        }
        return tmp;
    }

    private Player(String userID, String name, String discriminator, long tokenCount, long freeTokenCount, long lifeTimeWon, long listTimeLost, long totalBet, long firstSeen, long lastSeen) {
        this.userID = userID;
        this.name = name;
        this.discriminator = discriminator;
        this.tokenCount = tokenCount;
        this.freeTokenCount = freeTokenCount;
        this.lifeTimeWon = lifeTimeWon;
        this.listTimeLost = listTimeLost;
        this.totalBet = totalBet;
        this.firstSeen = firstSeen;
        this.lastSeen = lastSeen;
    }


    public String getName() {
        return this.name;
    }
}
