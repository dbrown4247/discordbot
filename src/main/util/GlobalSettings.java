package main.util;

import java.awt.*;

public class GlobalSettings {
    public static Color accentColor = new Color(100,0,175);
    public static String brandIcon = "http://i.imgur.com/sgEvet3.png";
    public static String defaultSeed = "#";
    public static boolean generateDefaultGameObjects = true;
    public static boolean generateDefaultGames = true;
    public static long initialTokenCount = 0;
    public static long initialFreeTokenCount = 0;
    public boolean canCashoutFreeTokens = false;
}
