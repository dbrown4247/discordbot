package main.util;

import main.bot.command.Command;
import main.bot.gambling.provablyfair.GameObject;
import main.bot.gambling.provablyfair.RandomData;
import main.util.data.GameLog;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameController {
    private static Map<String,GameObject> gameObjects = new HashMap<>();

    private static Map<String, Command> gameCommands = new HashMap<>();

    public static HashMap<String, List<RandomData<Integer>>> gameData = new HashMap<>();

    public static void setGameObjects(Map<String, GameObject> gameObjects){
        GameController.gameObjects = gameObjects;
    }

    public static void setGameCommands(Map<String, Command> gameCommands){
        GameController.gameCommands = gameCommands;
    }

    public static GameObject getGameObject(String name){
        return gameObjects.get(name);
    }

    public static Map<String, Command> getGameCommands(){
        return gameCommands;
    }

    public static List<RandomData<Integer>> getGameData(String partialHash){
        if(gameData.containsKey(partialHash)) {
            return gameData.get(partialHash);
        }else{
            return DataLoader.loadGame(partialHash).getRd();
        }
    }

    public static void logGame(String partialHash, List<RandomData<Integer>> rd) {
        gameData.put(partialHash, rd);
        DataLoader.writeGame(new GameLog(partialHash, rd));
    }
}
