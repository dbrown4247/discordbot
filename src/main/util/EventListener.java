package main.util;

import main.bot.command.Command;
import main.bot.command.commands.Proof;
import main.bot.command.commands.Seed;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventListener extends ListenerAdapter {
    private ArrayList<Command> commands = new ArrayList<Command>(){{
        add(new Seed());
        add(new Proof());
    }};
    private Map<String, Command> commandMap;


    public EventListener() {
        commandMap = new HashMap<>();
        for(Command i:commands){
            for(String i2:i.getPrefix()){
                commandMap.put(i2, i);
            }
        }
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent e) {
        String prefix = Command.getCmdPrefix(e.getMessage().getContent());
        List<Map<String, Command>> commandPools = new ArrayList<Map<String, Command>>(){{
            add(commandMap);
            add(GameController.getGameCommands());
        }};

        Command tmp = null;

        for(Map<String, Command> i: commandPools){
            if(tmp != null) break;
            tmp = i.get(prefix);
        }

        if(tmp != null){
            if(tmp.validate(e)){
                tmp.execute(e);
            }
        }
    }
}
